const mongoose = require('mongoose')

const SettingSchema = new mongoose.Schema({
    
    color:{
        type: String
    },
    logo:{
        type:String
    },
    language:{
        type:String
    },
    languages:{
        type: Object
    }
})

const Setting = mongoose.model("Setting",SettingSchema)

module.exports = Setting