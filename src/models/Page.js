const mongoose = require('mongoose');

const PageSchema = new mongoose.Schema({
    name : {
      type: String,
      required: true
    },
    slug : {
      type: String,
      required: true
    },
    content: {
      type: String,
      required: false
    },
    status:{
      type: String,
      default:"Active"
    },
    sequence:{
      type: Number,
      default:0
    }
})

const Page = mongoose.model("Page",PageSchema)

module.exports = Page