const mongoose = require('mongoose')

const CourseSchema = new mongoose.Schema({
    department_id : {
        type: String,
    },
    category : {
        type: String,
    },
    name : {
        type: String,
        // validate: {
        //     validator: async function(name) {
        //       const course = await this.constructor.findOne({ name });
        //       if(course) {
        //         if(this.id === course.id && this.language === course.language) {
        //           return true;
        //         }
        //         return false;
        //       }
        //       return true;
        //     },
        //     message: props => 'The specified course is already in use.'
        // },
        required: true
    },
    shortDescription:{
        type: String
    },
    logo:{
        type:String
    },
    logoTitle: {
        type: String
    },
    introduction:{
        type: String,
        required: true,
    },
    task:{
        type: String,
        required: true,
    },
    process:{
        type: String,
        required: true,
    },
    conclusion:{
        type: String,
        required: true,
    },
    learningObjectives:{
        type: String,
        required: true,
    },
    language: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    educator_details: {
        type: String,
        required: true
    },
    links:{
        type: String,
    },
    documents:{
        type: String,
    },
    videos:{
        type: String,
    },
    quiz:{
        type: String,
    },
    createdBy:{
        type: String,
    },
})

const Course = mongoose.model("Course",CourseSchema)

module.exports = Course