const mongoose = require('mongoose')

const DepartmentSchema = new mongoose.Schema({
    name : {
        type: String,
        // validate: {
        //     validator: async function(name) {
        //       const department = await this.constructor.findOne({ name });
        //       if(department) {
        //         if(this.id === department.id) {
        //           return true;
        //         }
        //         return false;
        //       }
        //       return true;
        //     },
        //     message: props => 'The specified department is already in use.'
        // },
        required: true
    },
    categories : [{
        name: {
                type: String,
                required: true
            }
    }],
    language: {
      type: String,
      required: true
    },
    status: {
        type: String,
        required: true
    },
    logo: {
        type: String
    },
})

const Department = mongoose.model("Department",DepartmentSchema)

module.exports = Department