const mongoose = require('mongoose')

const LanguageSchema = new mongoose.Schema({
    name : {
        type: String,
        validate: {
            validator: async function(name) {
              const language = await this.constructor.findOne({ name });
              if(language) {
                if(this.id === language.id) {
                  return true;
                }
                return false;
              }
              return true;
            },
            message: props => 'The specified langauge is already in use.'
        },
        required: true
    },
    logo: {
      type: String,
      required: true
    },
    status: {
        type: String,
        required: true
    },
})

const Language = mongoose.model("Language",LanguageSchema)

module.exports = Language