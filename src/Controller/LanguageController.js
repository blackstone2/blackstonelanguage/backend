const Language = require('../models/Language');
const multer = require("multer");
const path = require("path");

const cloudinary = require('cloudinary').v2;


cloudinary.config({
    cloud_name: "drrfy9x7f",
    api_key: "312674413883738",
    api_secret: "qumGZWJFScwr5qUYp-3nF9Iytxo"
});


const LanguageController = { 
    saveLanguage: async (req, res) => {
        try {
            const language = new Language(req.body);
            const time = new Date().getTime();
            
            await cloudinary.uploader
            .upload(req.body.logo,{public_id: time})
            .then((result) => {
                console.log("HERE")
                const url = cloudinary.url(time);
                console.log("url",url);

                language.logo = url;
                
            }).catch((err) => {
                    console.log(err);
            });
            await language.save();
            res.status(200).send({ "message": "Language added." });
        } catch (e) {
            console.log(e);
            res.status(400).send({"error":e});
        }
    },
    updateLanguage: async (req, res) => {
        try{
            const updates = Object.keys(req.body);
            const time = new Date().getTime();
            const language = await Language.findById(req.params.id)
    
            if(!language)
            {
                res.status(404).send({"error" : "Language not found"})
            }

            updates.forEach((update) => language[update] = req.body[update])
            if(!req.body.logo.includes('upload'))
            {
                await cloudinary.uploader
                    .upload(req.body.logo,{public_id: time})
                    .then((result) => {
                        const url = cloudinary.url(time);
                        language.logo = url;
                        language.save();
                        return res.status(200).send({"message" : "Language updated."})
                        
                    }).catch((err) => {
                            console.log(err);
                    });
            }
            else{
                
                await department.save();
                return res.status(200).send({"message" : "Language updated."})
            
            }
            
            
              
        }catch (e){
            console.log(e);
            res.status(400).send(e)
        }
    
    },
    getLanguage: async (req, res) => {
        try{
            const language = await Language.findById(req.params.id)
    
            if(!language)
            {
                return res.status(404).send({"error" : "Language not found"})
            }
            
            res.status(200).send({language})
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    getLanguages: async (req, res) => {
        try {
            let languages = await Language.find({});
            
            
            res.json({
                details: languages
            });
        } catch (e) {
            res.status(400).send(e);
        }
    },
    deleteLanguage: async (req, res) => {
        try{
            const langauge = await Language.findByIdAndDelete(req.params.id)
            if(!langauge)
            {
                res.status(404).send({"error" : "Language not found"})
            }
            res.status(200).send({"message" : "Language deleted."})
        }catch (e){
            res.status(400).send(e)
        }
    
    
    },
    getAllLanguage: async (req, res) => {
        try {
            let languages = await Language.find({'status':"Active"});
            
            
            res.json({
                details: languages
            });
        } catch (e) {
            res.status(400).send(e);
        }
    }
}
module.exports = LanguageController;
