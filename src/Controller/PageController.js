const Page = require('../models/Page');
const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");
const Department = require('../models/Department');
var nodemailer = require('nodemailer');


const JWT_SECRET = "AUTH_TOKEN";
const PageController = { 
    getPages: async (req, res) => {
        try{
            const pages = await Page.find({}).sort('sequence')
            
            res.json({
                details: pages,
            });
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    deletePage: async (req, res) => {
        try{
            const langauge = await Page.findByIdAndDelete(req.params.id)
            if(!langauge)
            {
                res.status(404).send({"error" : "Page not found"})
            }
            res.status(200).send({"message" : "Page deleted."})
        }catch (e){
            res.status(400).send(e)
        }
    
    
    },

    savePage: async (req, res) => {
        
        const page = new Page(req.body); 
        try {
            const pageData = await Page.findOne({"name":req.body.name});
            if(pageData)
            {
                return res.status(404).send({"error" : "Page Already Exists"})
            }
            await page.save();

            res.status(200).send({ "message": "Page added." });
            
        } catch (e) {
            res.status(400).send(e);
        }
    },
    updatePage: async (req, res) => {
        try{
            
            console.log(req.body);
            const page = await Page.findById(req.params.id)
    
            if(!page)
            {
                return res.status(404).send({"error" : "Page not found"})
            }

            // const pageData = await Page.findOne({"page":req.body.page,"_id": { $ne: req.params.id } });
            // if(pageData)
            // {
            //     return res.status(404).send({"error" : "Page Already Exists"})
            // }
            
            const updates = Object.keys(req.body);
            console.log(updates);
            updates.forEach((update) => page[update] = req.body[update])
            await page.save()
            res.status(200).send({"message" : "Page updated."})
                    
            
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    getPage: async (req, res) => {
        try{
            const page = await Page.findById(req.params.id)
    
            if(!page)
            {
                return res.status(404).send({"error" : "Page not found"})
            }
            
            res.status(200).send({page})
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    getPageById: async (req, res) => {
        try{
            let page;
            if(req.params.id == 'undefined')
            {
                page = await Page.findOne({'slug':'home'})
            }
            else{
                page = await Page.findOne({'slug':req.params.id})
            }

            if(!page)
            {
                return res.status(404).send({"error" : "Page not found"})
            }
            res.json({
                details: page,
            });
        }catch (e){
            res.status(400).send(e)
        }
    },
    getAllPages: async (req, res) => {
        try{
            let pages = [];
            
            pages = await Page.find({'status':"Active"}).sort('sequence');
            
            
            res.json({
                details: pages,
            });
        }catch (e){
            res.status(400).send(e)
        }
    },
    
}
module.exports = PageController;
