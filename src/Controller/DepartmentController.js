const Department = require('../models/Department');
const multer = require("multer");
const path = require("path");
const cloudinary = require('cloudinary').v2;


cloudinary.config({
    cloud_name: "drrfy9x7f",
    api_key: "312674413883738",
    api_secret: "qumGZWJFScwr5qUYp-3nF9Iytxo"
});


const DepartmentController = { 
    saveDepartment: async (req, res) => {

        try {
            const department = new Department(req.body);
            department.categories = [{
                    "name":"Beginner"
                },
                {
                    "name": "Intermediate"
                },
                {
                    "name":"Advanced"
                }
            ];
            console.log(req.body);
            const time = new Date().getTime();
            await cloudinary.uploader
            .upload(req.body.logo,{public_id: time})
            .then((result) => {
                console.log("HERE")
                const url = cloudinary.url(time);
                console.log("url",url);

                department.logo = url;
                
            }).catch((err) => {
                    console.log(err);
            });

            

            await department.save();
            res.status(200).send({ "message": "Department added." });


        }
        catch (e) {
            console.log(e);
            res.status(400).send(e);
            
        }

    },
    updateDepartment: async (req, res) => {
        try{
            

            const department = await Department.findById(req.params.id)
            const time = new Date().getTime();
            if(!department)
            {
                res.status(404).send({"error" : "Department not found"})
            }
            const updates = Object.keys(req.body);
            updates.forEach((update) => department[update] = req.body[update])
            if(!req.body.logo.includes('upload'))
            {
                await cloudinary.uploader
                    .upload(req.body.logo,{public_id: time})
                    .then((result) => {
                        console.log("HERE")
                        const url = cloudinary.url(time);
                        console.log("url",url);

                        department.logo = url;
                        department.save();
                        return res.status(200).send({"message" : "Department updated."})
                        
                    }).catch((err) => {
                            console.log(err);
                    });
            }
            else{
                
                await department.save();
                return res.status(200).send({"message" : "Department updated."})
            
            }
            
            
            
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    getDepartment: async (req, res) => {
        try{
            const department = await Department.findById(req.params.id)
    
            if(!department)
            {
                return res.status(404).send({"error" : "Department not found"})
            }
            
            res.status(200).send({department})
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    getDepartments: async (req, res) => {
        try {
            let departments = await Department.find({});
            
            
            res.json({
                details: departments,
            });
        } catch (e) {
            res.status(400).send(e);
        }
    },
    deleteDepartment: async (req, res) => {
        try{
            const langauge = await Department.findByIdAndDelete(req.params.id)
            if(!langauge)
            {
                res.status(404).send({"error" : "Department not found"})
            }
            res.status(200).send({"message" : "Department deleted."})
        }catch (e){
            res.status(400).send(e)
        }
    
    
    },
    getAllDepartments: async (req, res) => {
        try {
            let departments = await Department.find({"language":req.headers.language,'status':"Active"});
            
            res.json({
                details: departments,
            });
        } catch (e) {
            res.status(400).send(e);
        }
    },
    getDepartmentById: async (req, res) => {
        try{
            const department = await Department.findById(req.params.id)
            if(!department)
            {
                res.status(404).send({"error" : "Department not found"})
            }
            res.json({
                details: department,
            });
            
        }catch (e){
            res.status(400).send(e)
        }
    
    
    },
}
module.exports = DepartmentController;
