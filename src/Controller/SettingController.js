const Setting = require('../models/Setting');
const multer = require("multer");
const path = require("path");
const languageData = require("../../uploads/language.json");

const cloudinary = require('cloudinary').v2;


cloudinary.config({
    cloud_name: "drrfy9x7f",
    api_key: "312674413883738",
    api_secret: "qumGZWJFScwr5qUYp-3nF9Iytxo"
});



var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        
        cb(null, "uploads")
    },
    filename: function (req, file, cb) {
        let ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
        if(ext == ".json"){
            cb(null, "language"+ext)
        }
        else{
            cb(null, file.fieldname + "-" + Date.now()+"."+ext)
        }

    }
  })

    
var upload = multer({ 
    storage: storage,

  
}).fields('language'); 

const SettingController = { 
    
    saveSetting: async (req, res) => {
        try{
            const updates = Object.keys(req.body);
            const logoTime = new Date().getTime();
            const languageTime = new Date().getTime();

            let setting = await Setting.findOne({});
            
    
            if(setting == null)
            {
                setting = new Setting();
            }

            updates.forEach((update) => setting[update] = req.body[update])

            await cloudinary.uploader
            .upload(req.body.logo,{public_id: logoTime})
            .then((result) => {
                console.log("HERE")
                const url = cloudinary.url(logoTime);
                console.log("url",url);

                setting.logo = url;
                
            }).catch((err) => {
                    console.log(err);
            });

            await cloudinary.uploader
            .upload(req.body.language,{public_id: languageTime})
            .then((result) => {
                console.log("HERE")
                const url = cloudinary.url(languageTime);
                console.log("url",url);

                setting.language = url;
                
            }).catch((err) => {
                    console.log(err);
            });

            
            setting['languages'] = await require('../../uploads/language.json');
            
            console.log(setting);
            await setting.save();
            res.status(200).send({"message" : "Setting updated."})
                    
    
        }catch (e){
            console.log(e)
            res.status(400).send(e)
        }
    
    },
    getSetting: async (req, res) => {
        try {
            let settings = await Setting.findOne({});
            res.json({
                settings : {
                    languages : settings.languages,
                    language : settings.language,
                    color : settings.color,
                    logo : settings.logo,
                }
            });
        } catch (e) {
            res.status(400).send(e);
        }
    },
    saveImageEditor:async (req, res) => {
        try {
            const time = new Date().getTime();
            console.log(req.body);
            await cloudinary.uploader
            .upload(req.body.image,{public_id: time})
            .then((result) => {
                console.log("HERE")
                const url = cloudinary.url(time);
                
                return res.status(200).send({ url});    
            }).catch((err) => {
                    console.log(err);
            });
            
            
        } catch (e) {
            console.log(e);
            res.status(400).send({"error":e});
        }
    },
}
module.exports = SettingController;
