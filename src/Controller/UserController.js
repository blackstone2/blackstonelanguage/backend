const User = require('../models/User');
const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");
const Department = require('../models/Department');
var nodemailer = require('nodemailer');


const JWT_SECRET = "AUTH_TOKEN";
const UserController = { 
    register: async (req, res) => {
        let password = req.body.password.toString();

        bcrypt.hash(password, 10).then((hash) => {
            const user = new User({
                name: req.body.name,
                email: req.body.email,
                password: hash,
                type:req.body.type == undefined ? "Admin" : req.body.type
            });
            if(req.body.type == "Teacher")
            {
                user.status = "InActive";
            }
            user.save().then((response) => {
                if(req.body.type == "Teacher")
                {
                    return res.json({
                        message: "User successfully created!"
                    });
                }else{
                    return res.json({
                        message: "User successfully created!",
                        token: jsonwebtoken.sign({ user }, JWT_SECRET),
                        type: user.type,
                    });
                }
                
            }).catch(error => {
                res.status(500).json({
                    error: error
                });
            });
        });
    },
    login: async (req, res) => {
        try{
            let user;
            if(req.body.portal == "Admin"){
                user = await User.findOne({"email":req.body.email, type: { $in: [ "Super Admin","Admin", "Teacher" ] } })
            }

            if(req.body.portal == "Website"){
                user = await User.findOne({"email":req.body.email})
            }


            let password = req.body.password.toString();

            if(!user)
            {
                return res.status(404).send({"error" : "User not found"});
            }

            if(user.status != "Active")
            {
                return res.status(400).send({"error" : "User is Inactive."});
            }
            
            bcrypt.compare(password, user.password)
            .then(result => {
                if(result){
                    return res.json({
                        token: jsonwebtoken.sign({ user }, JWT_SECRET),
                        type: user.type,
                    });
                }
                else{
                    res.status(404).send({"error" : "Invalid Credentials"});    
                }
            })
            .catch(err => {
                res.status(404).send({"error" : "Invalid Credentials"});
            })
            
        }catch (e){
            
            res.status(400).send(e)
        }
    },
    recoverPassword: async (req, res) => {
        try{
            const user = await User.findOne({"email":req.body.email})
            if(!user)
            {
                return res.status(404).send({"error" : "User not found"});
            }

            

            var transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                  user: 'yashjethva2504@gmail.com',
                  pass: 'rmlkkdezujdfhaqp'
                }
            });
              
            var mailOptions = {
                from: 'yashjethva2504@gmail.com',
                to: 'yashjethva2504@gmail.com',
                subject: 'Reset Password - Cair4Youth',
                html: '<h3>Hello '+user.name +'</h3><br> <p>Reset Password Link : '+ process.env.websiteURL+'reset-password/?token='+Buffer.from(user.email).toString('base64')+'</p>'
            };
              
            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                  console.log(error);
                  res.status(404).send({"error" : "Somthing went wrong"})
                } else {
                  console.log('Email sent: ' + info.response);
                  res.status(200).send({"message" : "Password Reset Link sent on given link"})
                }
            });

        }catch (e){
            
            res.status(400).send(e)
        }
    },
    updatePassword: async (req, res) => {
        try{
            const user = await User.findOne({"email":req.body.email})
            if(!user)
            {
                return res.status(404).send({"error" : "User not found"});
            }

            bcrypt.hash(req.body.password, 10).then((hash) => {
                user.password = hash;
                user.save();
            });

            return res.json({
                message: "Password Updated!"
            });
            
        }catch (e){
            console.log(e)
            res.status(400).send(e)
        }
    
    },
    getUsers: async (req, res) => {
        try{
            const authHeader = req.headers.authorization;
            const token = authHeader.split(" ")[1];
            const { user } = jsonwebtoken.verify(token, JWT_SECRET);

            let users;
            if(user.type == "Super Admin"){
                users = await User.find({})
            }
            else
            {
                users = await User.find({'type': {$ne: "Super Admin"}})
            }
            
            res.json({
                details: users,
            });
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    activateUser: async (req, res) => {
        try{
            const user = await User.findById(req.params.id)
            
            user.status = "Active";
            await user.save();
            return res.json({
                message: "User successfully Activated!"
            });
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    getProfile: async (req, res) => {
        try {
            const authHeader = req.headers.authorization;
            const token = authHeader.split(" ")[1];
            const { user } = jsonwebtoken.verify(token, JWT_SECRET);
            const profile = await User.findById(user._id)
            res.status(200).send({user:profile})
        } catch (e) {
            res.status(400).send(e);
        }
    },
    saveProfile: async (req, res) => {
        try{
            const authHeader = req.headers.authorization;
            const token = authHeader.split(" ")[1];
            const { user } = jsonwebtoken.verify(token, JWT_SECRET);
            const updates = Object.keys(req.body);

            const profile = await User.findById(user._id)
    
            if(!profile)
            {
                res.status(404).send({"error" : "User not found"})
            }
    
            
            updates.forEach((update) => profile[update] = req.body[update])
            await profile.save()
            res.status(200).send({"message" : "Profile updated."})
          
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    deleteUser: async (req, res) => {
        try{
            const langauge = await User.findByIdAndDelete(req.params.id)
            if(!langauge)
            {
                res.status(404).send({"error" : "User not found"})
            }
            res.status(200).send({"message" : "User deleted."})
        }catch (e){
            res.status(400).send(e)
        }
    
    
    },

    saveUser: async (req, res) => {
        
        const user = new User(req.body); 
        try {
            let password = req.body.password.toString();
            const userData = await User.findOne({"email":req.body.email});
            if(userData)
            {
                return res.status(404).send({"error" : "Email Already Exists"})
            }

            bcrypt.hash(password, 10).then((hash) => {
                const user = new User({
                    name: req.body.name,
                    email: req.body.email,
                    password: hash,
                    type:req.body.type == undefined ? "Admin" : req.body.type
                });

                user.save().then((response) => {
                    
                    return res.json({
                        message: "User successfully created!",
                        token: jsonwebtoken.sign({ user }, JWT_SECRET),
                        type: user.type,
                    });
                }).catch(error => {
                    res.status(500).json({
                        error: error
                    });
                });
            });
        } catch (e) {
            res.status(400).send(e);
        }
    },
    updateUser: async (req, res) => {
        try{
            
            console.log(req.body);
            const user = await User.findById(req.params.id)
    
            if(!user)
            {
                return res.status(404).send({"error" : "User not found"})
            }

            const userData = await User.findOne({"email":req.body.email,"_id": { $ne: req.params.id } });
            if(userData)
            {
                return res.status(404).send({"error" : "Email Already Exists"})
            }
            
            const updates = Object.keys(req.body);
            console.log(updates);
            updates.forEach((update) => user[update] = req.body[update])
            await user.save()
            res.status(200).send({"message" : "User updated."})
                    
            
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    getUser: async (req, res) => {
        try{
            const user = await User.findById(req.params.id)
    
            if(!user)
            {
                return res.status(404).send({"error" : "USer not found"})
            }
            
            res.status(200).send({user})
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    
}
module.exports = UserController;
