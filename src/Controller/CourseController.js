const Course = require('../models/Course');
const Department = require('../models/Department');
const multer = require("multer");
const path = require("path");
const jsonwebtoken = require("jsonwebtoken");
const JWT_SECRET = "AUTH_TOKEN";

const cloudinary = require('cloudinary').v2;


cloudinary.config({
    cloud_name: "drrfy9x7f",
    api_key: "312674413883738",
    api_secret: "qumGZWJFScwr5qUYp-3nF9Iytxo"
});


const CourseController = { 
    saveCourse: async (req, res) => {
        const authHeader = req.headers.authorization;
        const token = authHeader.split(" ")[1];
        const { user } = jsonwebtoken.verify(token, JWT_SECRET);
        
        try {
            
            const course = new Course(req.body); 
            const department = await Department.findById(Object(req.body.department_id));
            course.language = department.language;
            course.createdBy = user._id;

            const time = new Date().getTime();
            
            await cloudinary.uploader
            .upload(req.body.logo,{public_id: time})
            .then((result) => {
                console.log("HERE")
                const url = cloudinary.url(time);
                console.log("url",url);

                course.logo = url;
                course.save();
                
            }).catch((err) => {
                    console.log(err);
            });
            
            
            res.status(200).send({ "message": "Course added." });
            
        } catch (e) {
            console.log(e)
            res.status(400).send(e);
        }
    },
    updateCourse: async (req, res) => {
        try{
            
            
            const time = new Date().getTime();

            const course = await Course.findById(req.params.id)
            const updates = Object.keys(req.body);
            updates.forEach((update) => course[update] = req.body[update])
            const department = await  Department.findById(course.department_id);
            if(!course)
            {
                res.status(404).send({"error" : "Course not found"})
            }

            if(!req.body.logo.includes('upload'))
            {
                await cloudinary.uploader
                    .upload(req.body.logo,{public_id: time})
                    .then((result) => {
                        const url = cloudinary.url(time);
                        course.logo = url;
                        
                        course['language'] = department.language;
                        course.save()
                        return res.status(200).send({"message" : "Course updated."})
                        
                    }).catch((err) => {
                            console.log(err);
                    });
            }
            else{
                console.log(course)
                course['language'] = department.language;
                await course.save()
                return res.status(200).send({"message" : "Course updated."})
            }
            
            
        }catch (e){
            console.log(e)
            res.status(400).send(e)
        }
    
    },
    getCourse: async (req, res) => {
        try{
            const course = await Course.findById(req.params.id)
    
            if(!course)
            {
                return res.status(404).send({"error" : "Course not found"})
            }
            
            res.status(200).send({course})
        }catch (e){
            res.status(400).send(e)
        }
    
    },
    getCourses: async (req, res) => {
        try {
            const authHeader = req.headers.authorization;
            const token = authHeader.split(" ")[1];
            const { user } = jsonwebtoken.verify(token, JWT_SECRET);
            let courses;
            if(user.type == "Teacher"){
                courses = await Course.find({"createdBy":user._id});
            }
            else{
                courses = await Course.find({});
            }
            
            res.json({
                details: courses
            });
        } catch (e) {
            res.status(400).send(e);
        }
    },
    deleteCourse: async (req, res) => {
        try{
            const langauge = await Course.findByIdAndDelete(req.params.id)
            if(!langauge)
            {
                res.status(404).send({"error" : "Course not found"})
            }
            res.status(200).send({"message" : "Course deleted."})
        }catch (e){
            res.status(400).send(e)
        }
    
    
    },
    getCourseById: async (req, res) => {
        try{
            const course = await Course.findById(req.params.id)
            if(!course)
            {
                res.status(404).send({"error" : "Course not found"})
            }
            res.json({
                details: course,
            });
        }catch (e){
            res.status(400).send(e)
        }
    },
    getAllCourses: async (req, res) => {
        try{
            let courses = [];
            
            if(req.params.id == "null")
            {
                if(req.body.length > 0)
                {
                    courses = await Course.find({'status':"Active","language":req.headers.language,category: {$in: req.body}});
                }else{
                    courses = await Course.find({'status':"Active","language":req.headers.language});
                }
            }
            else
            {
                
                if(req.body.length > 0)
                {
                    courses = await Course.find({'status':"Active",department_id:req.params.id,"language":req.headers.language,category: {$in: req.body}});
                }else{
                    courses = await Course.find({'status':"Active",department_id:req.params.id,"language":req.headers.language});
                }
            }
            
            
            res.json({
                details: courses,
            });
        }catch (e){
            res.status(400).send(e)
        }
    },
}
module.exports = CourseController;
