const express = require('express')
const router = new express.Router();
let LanguageController = require('../Controller/LanguageController');
let DepartmentController = require('../Controller/DepartmentController');
let CourseController = require('../Controller/CourseController');
let UserController = require('../Controller/UserController');
let PageController = require('../Controller/PageController');
let SettingController = require('../Controller/SettingController');

const auth = require("../middleware/auth");

//Login 
router.route('/login').post(UserController.login);

//Register
router.route('/register').post(UserController.register);

router.route('/recover-password').post(UserController.recoverPassword);

router.route('/update-password').post(UserController.updatePassword);

router.get('/users',auth,UserController.getUsers);
router.delete('/user/:id',auth,UserController.deleteUser);
router.get('/activate-user/:id',auth,UserController.activateUser);
router.get('/user/:id',auth,UserController.getUser);
router.post('/save-user',auth,UserController.saveUser);
router.post('/update-user/:id',auth,UserController.updateUser);

router.get('/profile',UserController.getProfile);
router.post('/profile',UserController.saveProfile);

//Language
router.get('/languages',auth,LanguageController.getLanguages);
router.get('/language/:id',auth,LanguageController.getLanguage);
router.post('/save-language',auth,LanguageController.saveLanguage);
router.post('/update-language/:id',auth,LanguageController.updateLanguage);
router.delete('/language/:id',auth,LanguageController.deleteLanguage);
router.get('/public-languages',LanguageController.getAllLanguage);

//Department
router.get('/departments',auth,DepartmentController.getDepartments);
router.get('/department/:id',auth,DepartmentController.getDepartment);
router.post('/save-department',auth,DepartmentController.saveDepartment);
router.post('/update-department/:id',auth,DepartmentController.updateDepartment);
router.delete('/department/:id',auth,DepartmentController.deleteDepartment);
router.get('/public-departments',DepartmentController.getAllDepartments);
router.get('/public-department/:id',DepartmentController.getDepartmentById);

//Course
router.get('/courses',auth,CourseController.getCourses);
router.get('/course/:id',auth,CourseController.getCourse);
router.post('/save-course',auth,CourseController.saveCourse);
router.post('/update-course/:id',auth,CourseController.updateCourse);
router.delete('/course/:id',auth,CourseController.deleteCourse);
router.post('/public-courses/:id',CourseController.getAllCourses);
router.get('/public-course/:id',CourseController.getCourseById);

router.get('/setting',SettingController.getSetting);
router.post('/setting',auth,SettingController.saveSetting);

router.post('/save-image-editor',SettingController.saveImageEditor);


router.get('/pages',auth,PageController.getPages);
router.delete('/page/:id',auth,PageController.deletePage);
router.get('/page/:id',auth,PageController.getPage);
router.post('/save-page',auth,PageController.savePage);
router.post('/update-page/:id',auth,PageController.updatePage);
router.get('/public-pages',PageController.getAllPages);
router.get('/public-page/:id',PageController.getPageById);


module.exports = router;

