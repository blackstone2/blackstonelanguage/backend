const mongoose = require('mongoose')
const mongodb = require('mongodb')

mongoose.connect('mongodb://127.0.0.1:27017/blackstone', {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
})