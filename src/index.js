const express = require('express')
require('./db/mongoose')
const router = require('./routers');
var cors = require('cors')
var bodyParser = require("body-parser");
const https = require('https')
const fs = require('fs')

let dotenv = require('dotenv').config()
console.log(dotenv);
const app = express();

app.use('/uploads', express.static('uploads'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const port = process.env.PORT || 3001

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(cors());
app.use('/', router);



// const options = {
//     cert: fs.readFileSync('/etc/letsencrypt/live/c4y.popaya.co.in/fullchain.pem'),
//     key: fs.readFileSync('/etc/letsencrypt/live/c4y.popaya.co.in/privkey.pem')
//   };
  
// https.createServer(options, app).listen(port,() => {
//     console.log("Server is running on port: ", port)
// });
app.listen(port,() => {
    console.log("Server is running on port: ", port)
})
